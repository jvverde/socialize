var $oauth_token = null;
//var $MSG = '<a href="http://socialize.serprest.pt/" rel="nofollow">Introduce me</a> to the society';
var $MSG = 'You can <a href="http://socialize.serprest.pt/" rel="nofollow">share me</a> if you wish';
if (typeof window.localStorage != 'undefined') $oauth_token = window.localStorage.getItem('oauth_token');
function doLogin($d){
	if ($d.stat == 'ok'){
		//console.log('You are login');
		//console.log($d);
		$('#user').html($d.user.username._content);
		$('body>div:not(.signed)').addClass('invisible');
		$('body>div.signed').removeClass('invisible');
		if (typeof window.localStorage != 'undefined') window.localStorage.setItem('oauth_token',$oauth_token);
		getPhotos(1);
	}else{
		console.log('Login Error');
	}
}
function login(){
	var $data = {
		api_key:'91dfebda66ccc3c7bd4ce52c0d0a0f5c',
		format:'json',
		jsoncallback: 'doLogin',
		method:'flickr.test.login'
	};
	loadAuthFromFlickr($data,function($d){
		console.log('Success function for login ',$d);
	});
}
function error($msg){
		var $b = $('<div class="button">Refresh</div>');
		$('body>div:not(#erro)').addClass('invisible');
		$('#erro').html($msg).append($b).removeClass('invisible');
		$b.click(function(){
			if (typeof window.localStorage != 'undefined') window.localStorage.removeItem('oauth_token');
			window.location.href = 'http://socialize.serprest.pt/?'+Math.random();
		});		
}
function loadAuthFromFlickr($data,$f){ //authorized requests
	var $auth_url = 'oauth/flickrAuth.fcg';
	if ($oauth_token) $data.oauth_token = $oauth_token;
	$data.url = 'http://api.flickr.com/services/rest/';
	$data._random_ = Math.random();
	$data._time_ = Date.now();
	$.ajax({
		dataType:'json',
		url:$auth_url,
		type:'GET',
		data: $data,
		success: function($d){
			if ($d.auth && $d.auth.request && $d.auth.request.oauth_token){
				$oauth_token = $d.auth.request.oauth_token;
				var $url = 'http://m.flickr.com/services/oauth/authorize?perms=write&oauth_token=' + $d.auth.request.oauth_token;
				//console.log('oauth_token url -> ',$url);
				$('#flickrLink').attr('href',$url);
				$('#askPermissions').removeClass('invisible').parents().removeClass('invisible');
			}else if($d.sign){
				console.log('$d.sign.url -> ',$d.sign.url);
				$.ajax({
					url:$d.sign.url,
					dataType:'jsonp',
					cache: true, //in order to avoid the extra parameter _=[TIMESTAMP] . We don't need it at all. Worst with auth it invalidate the signature
					jsonp: false,
					success: $f,
					error: function(h){
						if (h.status != 200)
							error(h.responseText + ' (' + h.status + ') ' + h.statusText);
					},
					complete: function(h){
						console.log("Done!!!", h);
						if (h.status == 200) $f(h);
					}
				});
			}else{
				error('Not expected response in authorization Process');
			}
		},
		error: function(h){
			error(h.responseText + ' (' + h.status + ') ' + h.statusText);
		}
	});
}
var whenDone = null;
function showPhotos($d){
	console.log('showPhotos ',$d);
	var $photos = $('#photos').empty();
	for(var $i in $d.photos.photo){
		var $photo = $d.photos.photo[$i];
		var $li = $('<li>').data('photo',$photo).html($photo.title);
		if($photo.description && $photo.description._content && $photo.description._content.match(/<a\s+href\s*=\s*"http:\/\/socialize.serprest.pt/)){
			$li.addClass('yes');
		}else if(!$photo.ispublic){
			$li.prepend('*');
		}
		$photos.append($li);
	}
	var $p = $('#pages').empty();
	for (var $i = 1; $i <= $d.photos.pages; $i++){
		if ($i == $d.photos.page){
			$p.append('<span class="current_page">'+$i+'</span>');
		}else{
			$p.append('<span class="page">'+$i+'</span>');
		}
	}
	if (typeof whenDone == 'function') whenDone();
}
function getPhotos($page){
	if (!$page) $page = 1;
	var $data = {
		api_key:'ded351aacb4965f1162f5b8d87207665',
		format:'json',
		jsoncallback:'showPhotos',
		method:'flickr.people.getPhotos',
		extras: 'description',
		per_page: 100,
		page:$page,
		user_id: 'me'
	};
	loadAuthFromFlickr($data,function($d){
		//console.log('$d ->' , $d);
		console.log('Success function for getPhotos ',$d);
	})
}

var $deferredDom = $.Deferred();
var $referrer = document.referrer;
function dummy($d){
	console.log('Dummy ',$d);
};
function photoInfo($d){
	console.log('photoInfo ',$d);
	if($d.stat == 'ok'){
		var $photoUrl = 'http://farm'
		+ $d.photo.farm
		+ '.staticflickr.com/'
		+ $d.photo.server
		+ '/'
		+ $d.photo.id
		+ '_'
		+ $d.photo.secret
		+ '.jpg';
		var $href = 'http://pinterest.com/pin/create/button/?url='+$referrer+'&media='+$photoUrl+'&title=' + $d.photo.title._content +'&description=' + $d.photo.title._content ;
		var $src = $photoUrl.replace('.jpg','_q.jpg');
		$('#photo').attr('src',$src);
		$('#pinit').attr('href',$href);
		var $href2 = 'http://www.facebook.com/sharer.php?u='+$referrer;
		$('#fb').attr('href',$href2);
		var $href3 = 'http://twitter.com/intent/tweet?url='+$referrer;
		$('#tw').attr('href',$href3);
		$('.share').removeClass('invisible');
		$('#wellcome').addClass('invisible');
	}else{
		$('#erro').html('I cannot get a valid public photo to share. Are you sure your photo is public?').removeClass('invisible');
	}
}

$(document).ready(function(){
	$deferredDom.resolve();
});
//console.log('location: ', location.href);
$.when($deferredDom).done(function(){
	console.log('$referrer',$referrer);
	if($referrer && $referrer.match('http://www.flickr.com/photos/[^/]+/([0-9]+)')){
		var $photoId = RegExp.$1;
		$('#wellcome').removeClass('invisible');
		console.log('$id',$photoId);
		$.ajax({
			url: 'http://api.flickr.com/services/rest/',
			data: {
				format:'json',
				jsoncallback: 'photoInfo',
				api_key:'ded351aacb4965f1162f5b8d87207665',
				method:'flickr.photos.getInfo',
				photo_id : $photoId
			},
			dataType:'jsonp',
			success: function($d){
				console.log("The sucess shoudn't be called");
			},
			error:function(h){
				if (h.status != 200)
					error(h.responseText + ' (' + h.status + ') ' + h.statusText, h);
			},
			complete: function(h){
				console.log("Done!!!", h);
			}
		});
	}else if (window.location.href.match(/#signed-(.+)$/)){
		$oauth_token = RegExp.$1;
		//console.log('$oauth_token',$oauth_token);
		login();
	}else{
		$('.intro').removeClass('invisible');
	}
	$('#doit').click(function(){
		$('#doit').hide('slow');
		login();
	})
	$('#photos').on('click','li:not(.yes)',function(){
		var $this = $(this);
		var $photo = $this.data('photo');
		var $data = {
			api_key:'ded351aacb4965f1162f5b8d87207665',
			format:'json',
			jsoncallback:'dummy',
			method:'flickr.photos.setMeta',
			photo_id:$photo.id,
			title:$photo.title,
			description: $photo.description._content + '\n\n' + $MSG
		};
		loadAuthFromFlickr($data,function($d){
				//console.log('$d ', $d);
				console.log('Success function for setMeta (Yes) ',$d);
				if ($d.status == 200){
					$photo.description._content = $data.description; //update local value
					$this.addClass('yes');
				}
		})
	})
	$('#photos').on('click','li.yes',function(){
		var $this = $(this);
		var $photo = $this.data('photo');
		var $re = new RegExp($MSG);
		console.log($MSG);
		console.log($re);
		console.log($photo.description._content);
		if ($photo.description._content.match($re)){
			var $d = $photo.description._content.replace($re,'');
			var $data = {
				api_key:'ded351aacb4965f1162f5b8d87207665',
				format:'json',
				jsoncallback:'dummy',
				method:'flickr.photos.setMeta',
				photo_id:$photo.id,
				title:$photo.title,
				description: $d
			};
			loadAuthFromFlickr($data,function($d){
				//console.log('$d ', $d);
				console.log('Success function for setMeta (No) ',$d);
				if ($d.status == 200) $this.removeClass('yes');
			})
		}else{
			$this.append($('<div class="erro">I found no share message on this photo. So the remove cannot be done automatically</div>')) 
		}
	})
	$('#pages').on('click','span.page',function(){
		var $p = $(this).text();
		//console.log('$p=',$p);
		getPhotos($p);
	});
	$('#doit4page').click(function(){
		$('#photos>li:not(.yes)').click();
	});
	$('#undoit4page').click(function(){
		$('#photos>li:.yes').click();
	});
	$('#doit4all').click(function(){
		function nextPage(){	
			whenDone = function(){
					$('#doit4all').click();
			}
			$('#pages>span.current_page+span.page').click();	
		}
		var $last = $('#photos>li:not(.yes)').last();
		if ($last.length > 0){
			$last.bind('click', nextPage);
			$('#doit4page').click();
		}else nextPage()
	});
	$('#undoit4all').click(function(){
		function nextPage(){	
			whenDone = function(){
					$('#undoit4all').click();
			}
			$('#pages>span.current_page+span.page').click();	
		}
		var $last = $('#photos>li.yes').last();
		if ($last.length > 0){
			$last.bind('click', nextPage);
			$('#undoit4page').click();
		}else nextPage()
	});
	$('#socialize').click(function(){
		window.location.href='http://socialize.serprest.pt/?q='+Math.random();
	})
	$('#logout').click(function(){
		window.location.href = 'oauth/flickrAuth.fcg?logout='+$oauth_token;
	})
});




